# Dad Jokes

## Install dependencies

To install dependencies, you will want to run `npm install` in three places.

* This folder (the root)
* The `./app` folder
* The `./web` folder

## Run the app

Running `npm start` from this folder (the root) will fire up both the api and web projects.

## Node version

This app was built using Node version 6.11.3. If there are problems running it, it might help to use that specific version.
