import React, { Component } from 'react';
import logo from '../dad-jokes.png';
import './App.css';

import DadJokes from '../dad-jokes';

class App extends Component {
    render() {
        return (
            <section className="section">
                <div className="container">
                    <h1 className="title">
                        Dad Jokes
                        <img src={logo} className="logo" alt="logo" />
                    </h1>
                    <DadJokes />
                </div>
            </section>
        );
    }
}

export default App;
