import React from 'react';

import SearchForm from './SearchForm';
import Loading from './Loading';
import JokeList from './JokeList';

export default function DadJokes(props) {
    return (
        <div>
            <SearchForm {...props} />
            <hr />
            <Loading {...props} />
            <JokeList {...props} />
        </div>
    );
}
