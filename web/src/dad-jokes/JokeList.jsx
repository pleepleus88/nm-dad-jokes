import React from 'react';

import NoJokes from './NoJokes';
import JokeItem from './JokeItem';

export default function JokeList({ isLoading, jokes }) {
    if (isLoading) {
        return null;
    }

    if (!jokes || jokes.length === 0) {
        return <NoJokes />;
    }

    return (
        <table className="table is-fullwidth is-striped">
            <thead>
                <tr>
                    <th>Joke</th>
                </tr>
            </thead>
            <tbody>
                {jokes.map(joke => <JokeItem joke={joke} key={joke.id} />)}
            </tbody>
        </table>
    );
}
