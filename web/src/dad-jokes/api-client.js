import axios from 'axios';

export async function getAllJokes() {
    const results = await callApi('/api/v1/dad-jokes');

    return results;
}

export async function searchJokes(searchTerm) {
    const results = await callApi(
        `/api/v1/dad-jokes/search?term=${searchTerm}`
    );

    return results;
}

async function callApi(url) {
    const response = await axios.get(url, {
        headers: {
            accept: 'application/json'
        }
    });

    return response.data;
}
