import React from 'react';

export default function JokeItem({ joke }) {
    return (
        <tr>
            <td>{joke.joke}</td>
        </tr>
    );
}
