import React from 'react';
import logo from '../dad-jokes.png';

import './Loading.css';

export default function Loading({ isLoading }) {
    if (!isLoading) {
        return null;
    }

    return (
        <div className="loading">
            <h2>Loading Jokes...</h2>
            <h3>
                <img src={logo} className="spinner fa-spin" alt="loading" />
            </h3>
        </div>
    );
}
