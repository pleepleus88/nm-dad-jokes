import React from 'react';

import DadJokes from './DadJokes';

import { getAllJokes, searchJokes } from './api-client';

export default class DadJokesContainer extends React.Component {
    state = {
        jokes: [],
        isLoading: true,
        searchTerm: ''
    };

    render() {
        return (
            <DadJokes
                {...this.state}
                onSearchTermChanged={this.handleSearchTermChanged}
                onSearchSubmit={this.handleSearchSubmit}
            />
        );
    }

    async componentDidMount() {
        const allResults = await getAllJokes();

        this.setState({
            isLoading: false,
            jokes: allResults
        });
    }

    handleSearchTermChanged = event => {
        event.preventDefault();

        this.setState({
            searchTerm: event.target.value
        });
    };

    handleSearchSubmit = async event => {
        event.preventDefault();

        this.setState({
            isLoading: true
        });

        const searchResults = await searchJokes(this.state.searchTerm);

        this.setState({
            isLoading: false,
            jokes: searchResults
        });
    };
}
