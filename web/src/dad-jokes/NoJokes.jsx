import React from 'react';

export default function NoJokes() {
    return <h2>You ain't got no jokes! Try a different search.</h2>;
}
