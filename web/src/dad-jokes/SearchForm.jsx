import React from 'react';

export default function SearchForm({
    searchTerm,
    onSearchTermChanged,
    onSearchSubmit
}) {
    return (
        <form onSubmit={onSearchSubmit}>
            <div className="field has-addons">
                <div className="control is-expanded">
                    <input
                        className="input"
                        type="text"
                        placeholder="Search jokes"
                        value={searchTerm}
                        onChange={onSearchTermChanged}
                    />
                </div>
                <div className="control">
                    <button type="submit" className="button is-info">
                        Search
                    </button>
                </div>
            </div>
        </form>
    );
}
