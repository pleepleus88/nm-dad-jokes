# api

The dad-jokes api was bootstrapped with the [express-no-stress yeoman generator](https://github.com/cdimascio/generator-express-no-stress). Unused items were removed after bootstrapping.

## Install It
```
npm install
```

## Run It
#### Run in *development* mode:

```
npm start
```

#### Run in *production* mode:

```
npm run compile
npm run prod
```

#### Run tests:

```
npm test
```

### Try It
* Point you're browser to [http://localhost:3001](http://localhost:3001)
* Invoke the example REST endpoint `curl http://localhost:3001/api/v1/dad-jokes`

