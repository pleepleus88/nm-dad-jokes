import Express from 'express';
import * as path from 'path';
import * as http from 'http';
import * as os from 'os';
import l from './logger';

const app = new Express();

export default class ExpressServer {
    constructor() {
        const root = path.normalize(`${__dirname}/../..`);
        app.set('appPath', `${root}client`);
    }

    use(routes) {
        routes(app);
        return this;
    }

    listen(port = process.env.PORT) {
        const welcome = p => () =>
            l.info(
                `up and running in ${process.env.NODE_ENV ||
                    'development'} @: ${os.hostname()} on port: ${p}}`
            );
        http.createServer(app).listen(port, welcome(port));
        return app;
    }
}
