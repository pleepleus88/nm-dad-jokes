import './common/env';
import Server from './common/server';
import dadJokes from './api/dad-jokes';

export default new Server()
    .use(app => app.use('/api/v1/dad-jokes', dadJokes))
    .listen(process.env.PORT);
