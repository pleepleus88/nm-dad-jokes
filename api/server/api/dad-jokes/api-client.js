import axios from 'axios';
import sortBy from 'lodash.sortby';

const apiUrl = 'https://icanhazdadjoke.com/';

let allResults;
let searchResults = [];

export async function getAllJokes() {
    if (allResults) {
        return allResults;
    }

    const response = await callApi(`${apiUrl}/search?limit=30`);

    allResults = sortResults(response);

    return allResults;
}

export async function searchJokes(searchTerm) {
    if (searchResults[searchTerm]) {
        return searchResults[searchTerm];
    }

    const response = await callApi(
        `${apiUrl}/search?limit=30&term=${searchTerm}`
    );

    searchResults[searchTerm] = sortResults(response);

    return searchResults[searchTerm];
}

function callApi(url) {
    return axios.get(url, {
        headers: {
            accept: 'application/json'
        }
    });
}

function sortResults(response) {
    return sortBy(response.data.results, ['joke']);
}
