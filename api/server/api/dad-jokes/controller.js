import { getAllJokes, searchJokes } from './api-client';

export class DadJokesController {
    async all(req, res) {
        const results = await getAllJokes();

        res.json(results);
    }

    async search(req, res) {
        const results = await searchJokes(req.query.term);

        res.json(results);
    }
}

export default new DadJokesController();
